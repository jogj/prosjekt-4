import React from "react";
import "react-native-gesture-handler";
import useCachedResources from "./hooks/useCachedResources";
import { Provider } from "react-redux";
import store from "./store/store";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { SearchScreen } from "./screens/SearchScreen";
import StudentCityScreen from "./screens/StudentCityScreen";
import AddReviewScreen from "./screens/AddReviewScreen";
import { TransitionSpec } from "@react-navigation/stack/lib/typescript/src/types";
import { SafeAreaProvider } from "react-native-safe-area-context";
import { HomeScreen } from "./screens/HomeScreen";

// Needed to make navigation typed, see: https://reactnavigation.org/docs/typescript/
export type StackParamList = {
  Home: undefined;
  Search: undefined;
  StudentCity: undefined;
  AddReview: undefined;
};

const Stack = createStackNavigator<StackParamList>();

/* This animation sets duration to 10, we had an issue with the screen flashing white for a short duration.
This is probably due to duration being set to a small amount (over the screens refresh-rate) */
const config: TransitionSpec = {
  animation: "timing",
  config: {
    duration: 10,
  },
};

/*
 * Main app
 */
export default function App() {
  const isLoadingComplete = useCachedResources(); // Loads all data we need from cached resources

  if (!isLoadingComplete) {
    return null;
  } else {
    return (
      <SafeAreaProvider>
        <Provider store={store}>
          <NavigationContainer>
            <Stack.Navigator
              screenOptions={{
                headerShown: false,
              }}
            >
              <Stack.Screen
                name="Home"
                component={HomeScreen}
                options={{
                  transitionSpec: { open: config, close: config }, // Adding transition animations
                }}
              />
              <Stack.Screen
                name="Search"
                component={SearchScreen}
                options={{
                  transitionSpec: { open: config, close: config },
                }}
              />
              <Stack.Screen
                name="StudentCity"
                component={StudentCityScreen}
                options={{
                  transitionSpec: { open: config, close: config },
                }}
              />
              <Stack.Screen
                name="AddReview"
                component={AddReviewScreen}
                options={{
                  transitionSpec: { open: config, close: config },
                }}
              />
            </Stack.Navigator>
          </NavigationContainer>
        </Provider>
      </SafeAreaProvider>
    );
  }
}
