import { RootState } from "../../store/store";
import { useStore } from "react-redux";
import { ActionCreatorsMapObject } from "redux";
import { useMemo } from "react";
import { BoundActions } from "./interfaces";
/*
 * Hook used to get actions which can be used on store. Takes in actions (which extends ActionCreatorsMapObject) and returns it as BoundActions.
 * BoundActions calls the action creator and immediately dispatches it to the store
 */
export const useActions = <M extends ActionCreatorsMapObject>(actions: M) => {
  const { dispatch, getState } = useStore<RootState>();
  const returnKeys = () =>
    Object.keys(actions).reduce<Partial<BoundActions<M>>>(
      (res, key) => ({
        ...res,
        [key]: (...args: any) => {
          const action = actions[key](...args);
          return typeof action === "function"
            ? action(dispatch, getState)
            : dispatch(action);
        },
      }),
      {}
    ) as BoundActions<M>;
  return useMemo(returnKeys, []);
};
