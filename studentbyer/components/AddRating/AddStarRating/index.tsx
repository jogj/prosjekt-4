import * as React from "react";
import Icon from "react-native-vector-icons/FontAwesome5";
import { View } from "react-native";

/*
 * Component which renders 5 clickable stars
 */
const AddStarRating = ({
  onPress,
  rating,
  size,
}: {
  onPress: (number: number) => void;
  rating: number;
  size: number;
}) => {
  const array = [1, 2, 3, 4, 5];
  return (
    <View style={{ flex: 1, flexDirection: "row" }}>
      {array.map((number) => {
        if (number <= rating) {
          return (
            <Icon
              name="star"
              solid
              color="#4FD1C5"
              size={size}
              key={number}
              onPress={() => onPress(number)}
            />
          );
        } else {
          return (
            <Icon
              name="star"
              color="#4FD1C5"
              size={size}
              key={number}
              onPress={() => onPress(number)}
            />
          );
        }
      })}
    </View>
  );
};

export default AddStarRating;
