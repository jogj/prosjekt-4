import * as React from "react";
import { View, Text } from "react-native";
import AddStarRating from "./AddStarRating";

/*
 * Component for adding rating to a value, for example "Vurdering pris"
 */
const AddRating = ({
  rating,
  text,
  setRating,
}: {
  rating: number;
  text: String;
  setRating: (number: number) => void;
}) => {
  return (
    <View
      style={{
        flex: 5,
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <View style={{ flex: 1, flexDirection: "column" }}>
        <Text
          style={{ color: "white", fontSize: 15, fontWeight: "bold" }}
        >{`Vurdering ${text}:`}</Text>
      </View>
      <View style={{ flex: 1.5, flexDirection: "column" }}>
        <AddStarRating rating={rating} size={40} onPress={setRating} />
      </View>
    </View>
  );
};

export default AddRating;
