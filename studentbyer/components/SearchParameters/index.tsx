import React, { useEffect, useCallback, useState } from "react";
import { useSelector } from "react-redux";
import { RootState } from "../../store/store";
import { useActions } from "../../hooks/useActions";
import { setFilter } from "../../store/filter/actions";
import { SearchBar } from "react-native-elements";
import { View } from "react-native";
import RNPickerSelect from "react-native-picker-select";
import { City } from "../interfaces";
import { Sort } from "../../store/filter/interfaces";
import { setPage } from "../../store/page/actions";
import { StyleSheet } from "react-native";
import axios from "axios";
import { API } from "../../constants/AppConstants";
import debounce from "lodash.debounce";

/*
 * Component for giving input
 * Contains searchfield, choicebox for sorting and choicebox for filtering
 * Updates state.filterState.filter in redux on change
 */
export const SearchParameters = () => {
  const { filter } = useSelector((state: RootState) => {
    return {
      filter: state.filterState.filter,
    };
  });

  const [cities, setCities] = useState<City[]>([]);
  const actions = useActions({ setFilter, setPage });
  useEffect(() => {
    actions.setFilter({ ...filter, sort: "alphabetical" });
    axios.get(`${API}/byer`).then((res) => {
      setCities([...res.data]);
    });
  }, []);

  const updateSearch = (value: string) => {
    actions.setPage(0);
    actions.setFilter({
      ...filter,
      queryString: value,
    });
  };

  const debouncedSave = useCallback(
    //debounce on search - filter is only updated after 0,3 seconds of no writing in searchfield
    debounce((nextValue) => updateSearch(nextValue), 300),
    []
  );

  const handleSearch = (value: string) => {
    setSearch(value);
    debouncedSave(value);
  };

  const updateSort = (value: Sort) => {
    actions.setFilter({ ...filter, sort: value });
    actions.setPage(0);
  };
  const updateCity = (value: string) => {
    actions.setFilter({ ...filter, city: value });
    actions.setPage(0);
  };

  const [search, setSearch] = React.useState("");

  const sortValues = [
    { label: "Alfabetisk A -> Å", value: "alphabetical" },
    { label: "Alfabetisk Å -> A", value: "inverseAlphabetical" },
    { label: "Total vurdering høy -> lav", value: "ratingHighToLow" },
    { label: "Total vurdering lav -> høy", value: "ratingLowToHigh" },
  ];
  const cityValues = cities.map((city: City) => {
    return { label: city.navn, value: city.id };
  });

  return (
    <View style={{ minHeight: 75 }}>
      <View>
        <SearchBar
          containerStyle={{
            backgroundColor: "#2D3748",
            borderBottomColor: "transparent",
            borderTopColor: "transparent",
          }}
          placeholder="Søk etter studentby..."
          value={search}
          onChangeText={(nextValue) => handleSearch(nextValue)}
        />
      </View>
      <View style={styles.select}>
        <RNPickerSelect
          onValueChange={(value) => updateSort(value as Sort)}
          placeholder={sortValues[0]}
          style={pickerStyle}
          items={sortValues.slice(1, 4)}
        />
      </View>
      <View style={styles.select}>
        <RNPickerSelect
          onValueChange={(value) => updateCity(value)}
          placeholder={{ label: "Alle byer", value: "" }}
          style={pickerStyle}
          items={cityValues}
        />
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    width: "100%",
  },
  select: {
    marginVertical: 5,
    marginHorizontal: 10,
  },
});

const pickerStyle = {
  inputIOS: {
    color: "white",
    paddingTop: 13,
    paddingHorizontal: 10,
    paddingBottom: 12,
    backgroundColor: "#777777",
  },
  inputAndroid: {
    color: "white",
    backgroundColor: "#999999",
  },
  placeholder: {
    color: "white",
  },
  underline: { borderTopWidth: 0 },
};
