import * as React from "react";
import Icon from "react-native-vector-icons/FontAwesome5";
import { View } from "react-native";

type StarType = "FULL" | "HALF" | "EMPTY";

/*
 * Component for displaying a star rating
 */
const StarRating = ({
  rating,
  size,
  bgcolor,
}: {
  rating: number;
  size: number;
  bgcolor: string;
}) => {
  const arr: StarType[] = [];
  for (let i = 0; i < 5; i++) {
    const diff = rating - i;
    if (diff > 0.75) {
      arr.push("FULL");
    } else if (diff <= 0.75 && diff >= 0.25) {
      arr.push("HALF");
    } else {
      arr.push("EMPTY");
    }
  }
  return (
    <View style={{ flex: 1, flexDirection: "row", backgroundColor: bgcolor }}>
      {arr.map((star, index) => {
        switch (star) {
          case "FULL":
            return (
              <Icon name="star" solid color="#4FD1C5" size={size} key={index} />
            );
          case "HALF":
            return (
              <Icon
                name="star-half-alt"
                color="#4FD1C5"
                size={size}
                key={index}
              />
            );
          case "EMPTY":
            return <Icon name="star" color="#4FD1C5" size={size} key={index} />;
        }
      })}
    </View>
  );
};

export default StarRating;
