import * as React from "react";
import { View } from "react-native";
import { Text } from "react-native-elements";
import StarRating from "./StarRating";

/*
 * Component for displaying a star rating for a specific value, for example "Vurdering pris"
 */
const Rating = ({
  rating,
  text,
  bgcolor,
}: {
  rating: number;
  text: String;
  bgcolor: string;
}) => {
  return (
    <View
      style={{
        flex: 5,
        flexDirection: "row",
        justifyContent: "center",
      }}
    >
      <View
        style={{
          flex: 2,
          flexDirection: "column",
          paddingLeft: 10,
          backgroundColor: bgcolor,
        }}
      >
        <Text
          style={{ color: "white", fontSize: 15, fontWeight: "bold" }}
        >{`Vurdering ${text}:`}</Text>
      </View>
      <View style={{ flex: 1.5, flexDirection: "column" }}>
        <StarRating rating={rating} size={20} bgcolor={bgcolor} />
      </View>
      <View
        style={{ flex: 1, flexDirection: "column", backgroundColor: bgcolor }}
      >
        <Text style={{ color: "white" }}>{`(${rating.toFixed(2)})`}</Text>
      </View>
    </View>
  );
};

export default Rating;
