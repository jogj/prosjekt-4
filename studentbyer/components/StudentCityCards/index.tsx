import React, { useState, useEffect } from "react";
import { FlatList, Text, View } from "react-native";
import { useSelector } from "react-redux";
import { RootState } from "../../store/store";
import StudentCityCard from "./StudentCityCard";
import { useActions } from "../../hooks/useActions";
import { setPage } from "../../store/page/actions";
import { OFFSET, API } from "../../constants/AppConstants";
import { StudentCity } from "../../store/studentCity/interfaces";
import axios from "axios";
import { Loader } from "../Loader";
import { SearchScreenNavigationProp } from "../../screens/SearchScreen";

/*
 * Component for showing a list of all fetched studentcities
 * Fetches studentcities on when bottom of list is reached or filter is changed
 */
export default function StudentCityCards({
  navigation
}: {
  navigation: SearchScreenNavigationProp;
}) {
  const { page, filter } = useSelector((state: RootState) => {
    return {
      page: state.pageState,
      filter: state.filterState.filter
    };
  });

  const [studentCities, setStudentCities] = useState<StudentCity[]>([]);
  const [count, setCount] = useState(0); // Total count of StudentCities matching filter in database
  const [loading, setLoading] = useState(false);
  const [loadingMore, setLoadingMore] = useState(false);

  const actions = useActions({ setPage });

  /* This function handles what the component should do when the user has scrolled into the EndReachedThreshold */
  const handleLoadMore = () => {
    if (loadingMore) return; // If we are still trying to fetch StudentCities we should not change the page!
    if ((page + 1) * OFFSET < count) {
      // We should only change the page if there are more StudentCities to fetch
      actions.setPage(page + 1);
    }
  };

  /* Appends the data to the state */
  const appendStudentCities = (data: StudentCity[]) => {
    setStudentCities([...studentCities, ...data]);
  };

  /* Fetches studentcities using filter and page from Redux, then appends or sets the data using the funct argument */
  const loadStudentCities = (funct: (data: StudentCity[]) => void) => {
    return axios
      .get(`${API}/studentbyer`, {
        params: {
          take: OFFSET,
          skip: page * OFFSET,
          sort: filter.sort,
          querystring: filter.queryString,
          filter: filter.city
        }
      })
      .then(function(response) {
        funct([...response.data.studentbyer]);
        setCount(response.data.count);
        setLoadingMore(false);
        setLoading(false);
      })
      .catch(error => {
        setLoading(false);
        setLoadingMore(false);
      });
  };

  /* Effect for appending on page changes or setting studentcities on filter changes */
  useEffect(() => {
    if (page > 0) {
      setLoadingMore(true);
      loadStudentCities(appendStudentCities);
    } else {
      setLoading(true);
      loadStudentCities(setStudentCities);
    }
  }, [page, filter]);

  /* Footer component which should be loading if we are trying to fetch more studentcities */
  const renderFooter = () => {
    if (!loadingMore) return null;
    return <Loader />;
  };

  if (loading) {
    return <Loader />;
  }
  if (studentCities.length === 0) {
    return (
      <View style={{ flex: 1, justifyContent: "center", flexDirection: "row" }}>
        <Text style={{ marginTop: 40, fontSize: 20, color: "white" }}>
          Ingen studentbyer samsvarer med søket
        </Text>
      </View>
    );
  }
  return (
    <View>
      <FlatList
        onEndReached={() => handleLoadMore()}
        onEndReachedThreshold={0.5}
        data={studentCities}
        keyExtractor={item => item.id.toString()}
        initialNumToRender={OFFSET}
        ListFooterComponent={renderFooter}
        renderItem={({ item }) => (
          <View style={{ margin: 10 }}>
            <StudentCityCard
              key={item.id}
              studentCity={item}
              navigation={navigation}
            />
          </View>
        )}
      />
    </View>
  );
}
