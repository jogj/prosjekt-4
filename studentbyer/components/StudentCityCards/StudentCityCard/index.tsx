import React from "react";
import { StudentCity } from "../../../store/studentCity/interfaces";
import { Text, View } from "react-native";
import { Badge } from "react-native-elements";
import { useActions } from "../../../hooks/useActions";
import { setStudentCity } from "../../../store/studentCity/actions";
import { TouchableOpacity } from "react-native-gesture-handler";
import Rating from "../../Rating";

type Props = {
  studentCity: StudentCity;
  navigation: any;
};

/*
 * Component for showing data for one student city given as a prop.
 */
const StudentCityCard = ({ studentCity, navigation }: Props) => {
  const actions = useActions({ setStudentCity });

  return (
    <TouchableOpacity
      key={studentCity.id}
      onPress={() => {
        actions.setStudentCity(studentCity); // don't need to fetch again here
        navigation.navigate("StudentCity");
      }}
    >
      <View
        style={{
          flex: 5,
          flexDirection: "column",
          width: "100%",
          height: "100%",
          backgroundColor: "#4A5568",
          borderColor: "#4A5568",
          borderRadius: 5,
          paddingVertical: 20,
        }}
      >
        <View
          style={{
            flex: 1,
            flexDirection: "row",
            justifyContent: "center",
            backgroundColor: "#4A5568",
          }}
        >
          <Text style={{ color: "white", fontSize: 23 }}>
            {studentCity.navn}
          </Text>
        </View>
        <View
          style={{
            flex: 1,
            flexDirection: "row",
            padding: 9,
            backgroundColor: "#4A5568",
          }}
        >
          <Badge
            value={studentCity.byNavn}
            badgeStyle={{
              backgroundColor: "#319795",
              borderColor: "#1D4044",
            }}
            textStyle={{ fontSize: 15 }}
          />
        </View>
        <View style={{ flex: 1, flexDirection: "row" }}>
          <Rating
            text={"total"}
            rating={Number(studentCity.vurderingTotal)}
            bgcolor="#4A5568"
          />
        </View>
      </View>
    </TouchableOpacity>
  );
};
export default StudentCityCard;
