import React from "react";
import { View, ActivityIndicator } from "react-native";

/*
 * Loading component
 */
export const Loader = () => {
  return (
    <View
      style={{
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        position: "relative",
        width: "100%",
        paddingVertical: 20,
        marginBottom: 10
      }}
    >
      <ActivityIndicator animating size="large" color="#999999" />
    </View>
  );
};
