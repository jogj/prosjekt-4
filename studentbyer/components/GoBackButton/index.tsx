import * as React from "react";
import { Button } from "react-native-elements";
import Icon from "react-native-vector-icons/FontAwesome5";

/*
 * Button component which makes the app navigate back to the previous screen
 */
const GoBackButton = ({ navigation }: any) => {
  return (
    <Button
      onPress={() => navigation.goBack()}
      buttonStyle={{ borderColor: "#4FD1C5", borderWidth: 3 }}
      titleStyle={{ color: "#4FD1C5" }}
      title={" Gå tilbake"}
      icon={<Icon name="chevron-left" solid color="#4FD1C5" size={20} />}
      type="outline"
    />
  );
};

export default GoBackButton;
