import { SetPageAction, SET_PAGE } from "./actionTypes";

export function setPage(page: number): SetPageAction {
  return {
    type: SET_PAGE,
    page,
  };
}
