import { SetPageAction, SET_PAGE } from "./actionTypes";

export function pageReducer(
  state = 0, // Initial state
  action: SetPageAction
): number {
  switch (action.type) {
    case SET_PAGE: {
      return action.page;
    }
    default:
      return state;
  }
}
