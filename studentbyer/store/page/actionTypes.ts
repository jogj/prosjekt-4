export const SET_PAGE = "SET_PAGE";

export interface SetPageAction {
  type: typeof SET_PAGE;
  page: number;
}
