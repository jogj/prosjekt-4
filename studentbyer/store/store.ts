import { createStore, applyMiddleware } from "redux";
import createSagaMiddleware from "redux-saga";
import { composeWithDevTools } from "redux-devtools-extension";
import { all, takeLatest } from "@redux-saga/core/effects";
import { listenToFetchStudentCity } from "./sagas/studentCitySaga";
import { FETCH_STUDENT_CITY } from "./studentCity/actionTypes";
import { combineReducers } from "redux";
import { studentCityReducer } from "./studentCity/reducers";
import { filterReducer } from "./filter/reducers";
import { pageReducer } from "./page/reducers";

const rootReducer = combineReducers({
  studentCityState: studentCityReducer,
  filterState: filterReducer,
  pageState: pageReducer,
});
export type RootState = ReturnType<typeof rootReducer>;

const sagaMiddleware = createSagaMiddleware();
export default createStore(
  rootReducer,
  composeWithDevTools(applyMiddleware(sagaMiddleware))
);

/* Listens to FETCH_STUDENT_CITY and applies the correct saga */
function* sagas() {
  yield all([takeLatest(FETCH_STUDENT_CITY, listenToFetchStudentCity)]);
}
// Register the sagas:
sagaMiddleware.run(sagas);
