import { StudentCity } from "./interfaces";

export const FETCH_STUDENT_CITY = "FETCH_STUDENT_CITY";
export const PENDING_STUDENT_CITY = "PENDING_STUDENT_CITY";
export const SET_STUDENT_CITY = "SET_STUDENT_CITY";
export const FAILURE_STUDENT_CITY = "FAILURE_STUDENT_CITY";

export interface FetchStudentCityAction {
  type: typeof FETCH_STUDENT_CITY;
  id: Number;
}

export interface PendingStudentCityAction {
  type: typeof PENDING_STUDENT_CITY;
}

export interface SetStudentCityAction {
  type: typeof SET_STUDENT_CITY;
  studentCity: StudentCity;
}

export interface FailureStudentCityAction {
  type: typeof FAILURE_STUDENT_CITY;
}

export type StudentCityAction =
  | FetchStudentCityAction
  | PendingStudentCityAction
  | SetStudentCityAction
  | FailureStudentCityAction;
