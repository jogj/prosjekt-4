import {
  StudentCityAction,
  SET_STUDENT_CITY,
  FAILURE_STUDENT_CITY,
  PENDING_STUDENT_CITY,
} from "./actionTypes";
import { StudentCityState } from "./interfaces";

const INITIAL_STUDENT_CITIES_STATE: StudentCityState = {
  studentCity: { phase: "NOT_ASKED" },
};

export function studentCityReducer(
  state = INITIAL_STUDENT_CITIES_STATE,
  action: StudentCityAction
): StudentCityState {
  switch (action.type) {
    case SET_STUDENT_CITY: {
      return {
        ...state,
        studentCity: {
          phase: "SUCCESS",
          data: action.studentCity,
        },
      };
    }
    case FAILURE_STUDENT_CITY: {
      return {
        ...state,
        studentCity: { phase: "FAILURE", error: null },
      };
    }
    case PENDING_STUDENT_CITY: {
      return {
        ...state,
        studentCity: {
          phase: "PENDING",
        },
      };
    }
    default:
      return state;
  }
}
