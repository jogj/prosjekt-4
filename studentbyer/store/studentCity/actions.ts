import { StudentCity } from "./interfaces";
import {
  FAILURE_STUDENT_CITY,
  FETCH_STUDENT_CITY,
  PENDING_STUDENT_CITY,
  SET_STUDENT_CITY,
  StudentCityAction,
} from "./actionTypes";

// Inspired by: https://medium.com/unpacking-trunk-club/using-redux-and-redux-saga-to-handle-api-calls-18964d234660

export function fetchStudentCity(id: Number): StudentCityAction {
  return {
    type: FETCH_STUDENT_CITY,
    id,
  };
}
export function pendingStudentCity(): StudentCityAction {
  return {
    type: PENDING_STUDENT_CITY,
  };
}
export function setStudentCity(data: StudentCity): StudentCityAction {
  return {
    type: SET_STUDENT_CITY,
    studentCity: data,
  };
}
export function failureStudentCity(): StudentCityAction {
  return {
    type: FAILURE_STUDENT_CITY,
  };
}
