export default interface Filter {
  queryString: string;
  sort: Sort;
  city: string;
}
export interface FilterState {
  filter: Filter;
}

export type Sort =
  | "alphabetical"
  | "inverseAlphabetical"
  | "ratingHighToLow"
  | "ratingLowToHigh"
  | "";
