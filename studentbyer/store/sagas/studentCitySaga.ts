import { API } from "../../constants/AppConstants";
import axios from "axios";
import { call, put } from "@redux-saga/core/effects";
import { RootState } from "../store";
import {
  pendingStudentCity,
  setStudentCity,
  failureStudentCity
} from "../studentCity/actions";

/* Function who listens to fetchStudentCity actions and then dispatches the correct actions and fetches from api */
export function* listenToFetchStudentCity(action: { id: any }) {
  yield put(pendingStudentCity());
  try {
    const id = action.id;
    const { data } = yield call(axios.get, `${API}/studentbyer/${id}`);
    yield put(setStudentCity(data));
  } catch (error) {
    yield put(failureStudentCity());
  }
}
