export type RemoteDataT<D, E> =
  | { phase: "NOT_ASKED" }
  | { phase: "PENDING" }
  | { phase: "SUCCESS"; data: D }
  | { phase: "FAILURE"; error: E | null };
