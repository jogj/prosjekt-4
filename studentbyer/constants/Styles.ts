import { StyleSheet } from "react-native";

const globalStyles = StyleSheet.create({
  statusBar: {
    flex: 1,
    backgroundColor: "#2D3748",
  },
  container: {
    justifyContent: "center",
    width: "100%",
    height: "100%",
    backgroundColor: "#2D3748",
  },
});

export default globalStyles;
