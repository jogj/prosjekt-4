import * as React from "react";
import { Text, View } from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import globalStyles from "../constants/Styles";
import Icon from "react-native-vector-icons/FontAwesome5";
import { Button } from "react-native-elements";
import { StackNavigationProp } from "@react-navigation/stack";
import { StackParamList } from "../App";

export type HomeScreenNavigationProp = StackNavigationProp<
  StackParamList,
  "Home"
>;

/*
 * Screen which introduces the user to the application
 */
export const HomeScreen = ({
  navigation,
}: {
  navigation: HomeScreenNavigationProp;
}) => {
  return (
    <>
      <SafeAreaView style={globalStyles.statusBar}>
        <View
          style={{
            flex: 4,
            justifyContent: "center",
            flexDirection: "row",
            alignItems: "flex-end",
          }}
        >
          <Icon name="home" solid color="#4FD1C5" size={200} />
        </View>
        <View
          style={{
            flex: 1,
            justifyContent: "center",
            flexDirection: "row",
          }}
        >
          <Text
            style={{
              color: "#4FD1C5",
              fontSize: 40,
              fontWeight: "bold",
              fontFamily: "space-mono",
            }}
          >
            STUDENTBOLIGER
          </Text>
        </View>
        <View
          style={{
            flex: 2,
            flexDirection: "row",
            paddingHorizontal: 10,
          }}
        >
          <Text style={{ textAlign: "center", fontSize: 18, color: "white" }}>
            Studentboliger er en app for deg som leter etter den perfekte
            studentboligen! Her kan du se gjennom studentbyer basert på
            vurderinger utifra pris, lokasjon, fellesareal og tilstand og legge
            igjen din egen vurdering
          </Text>
        </View>
        <View
          style={{ flex: 2, flexDirection: "row", justifyContent: "center" }}
        >
          <Button
            containerStyle={{ width: 360 }}
            buttonStyle={{ borderColor: "#4FD1C5", borderWidth: 3 }}
            titleStyle={{ fontSize: 30, color: "#4FD1C5" }}
            title={"Finn din studentbolig nå!"}
            onPress={() => navigation.navigate("Search")}
            type="outline"
          />
        </View>
      </SafeAreaView>
    </>
  );
};
