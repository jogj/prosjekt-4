import * as React from "react";
import { useSelector } from "react-redux";
import { Text, View } from "react-native";
import { RootState } from "../store/store";
import { Button } from "react-native-elements";
import AddRating from "../components/AddRating";
import { useState } from "react";
import Toast from "react-native-tiny-toast";
import axios from "axios";
import { API } from "../constants/AppConstants";
import { useActions } from "../hooks/useActions";
import { setPage } from "../store/page/actions";
import { SafeAreaView } from "react-native-safe-area-context";
import globalStyles from "../constants/Styles";
import GoBackButton from "../components/GoBackButton";
import { fetchStudentCity } from "../store/studentCity/actions";
import { Loader } from "../components/Loader";
import { StackNavigationProp } from "@react-navigation/stack";
import { StackParamList } from "../App";

type AddReviewScreenNavigationProp = StackNavigationProp<
  StackParamList,
  "AddReview"
>;

/*
 * AddReviewScreen for adding reviews to a studentcity
 */
const AddReviewScreen = ({
  navigation
}: {
  navigation: AddReviewScreenNavigationProp;
}) => {
  const { remoteData } = useSelector((state: RootState) => {
    return {
      remoteData: state.studentCityState.studentCity
    };
  });

  const studentCity = remoteData.phase == "SUCCESS" ? remoteData.data : null; // Get the current studentcity

  const actions = useActions({ setPage, fetchStudentCity });

  const [vurderingPris, setVurderingPris] = useState(0);
  const [vurderingFellesAreal, setVurderingFellesAreal] = useState(0);
  const [vurderingLokasjon, setVurderingLokasjon] = useState(0);
  const [vurderingTilstand, setVurderingTilstand] = useState(0);
  const [error, setError] = useState("");

  /* Handles submit of a review */
  const submit = () => {
    if (
      // All numbers should be set
      vurderingLokasjon == 0 ||
      vurderingFellesAreal == 0 ||
      vurderingTilstand == 0 ||
      vurderingPris == 0
    ) {
      setError("Du må gi mellom 1-5 stjerner på alle");
    } else {
      studentCity &&
        axios
          .post(`${API}/studentbyer/${studentCity.id}/anmeldelser`, {
            vurderingLokasjon,
            vurderingFellesAreal,
            vurderingTilstand,
            vurderingPris,
            studentby: studentCity.id
          })
          .then(() => {
            actions.fetchStudentCity(studentCity.id);
            actions.setPage(0); // Needed to reload the studentcities-list so that the data there is also up to date
            navigation.goBack();
            showSuccessToast();
          })
          .catch(err => showErrorToast(err));
    }
  };
  const showSuccessToast = () => {
    Toast.showSuccess("Din vurdering har blitt lagt til!");
  };

  const showErrorToast = (err: String) => {
    Toast.show(`Error: ${err}`);
  };
  if (studentCity) {
    return (
      <SafeAreaView style={globalStyles.statusBar}>
        <View style={globalStyles.container}>
          <View style={{ flex: 1, flexDirection: "column" }}>
            <View
              style={{
                flex: 2,
                alignItems: "center",
                flexDirection: "row",
                justifyContent: "flex-start"
              }}
            >
              <GoBackButton navigation={navigation} />
            </View>
            <View
              style={{
                flex: 2,
                flexDirection: "column",
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <View
                style={{
                  flex: 1,
                  flexDirection: "row"
                }}
              >
                <Text
                  style={{
                    fontSize: 25,
                    fontWeight: "bold",
                    color: "white",
                    textAlign: "center"
                  }}
                >
                  {`Ny vurdering av \n ${studentCity.navn}`}
                </Text>
              </View>
            </View>
            <View
              style={{
                flex: 8,
                flexDirection: "column",
                alignItems: "center"
              }}
            >
              <AddRating
                rating={vurderingPris}
                text={"pris"}
                setRating={setVurderingPris}
              />
              <AddRating
                rating={vurderingFellesAreal}
                text={"fellesareal"}
                setRating={setVurderingFellesAreal}
              />
              <AddRating
                rating={vurderingLokasjon}
                text={"lokasjon"}
                setRating={setVurderingLokasjon}
              />
              <AddRating
                rating={vurderingTilstand}
                text={"tilstand"}
                setRating={setVurderingTilstand}
              />
            </View>
            <View
              style={{
                flex: 2,
                flexDirection: "column",
                justifyContent: "center",
                alignItems: "center",
                paddingTop: 40
              }}
            >
              <View style={{ flex: 1, flexDirection: "row" }}>
                <Text
                  style={{ fontSize: 15, fontWeight: "bold", color: "red" }}
                >
                  {error}
                </Text>
              </View>
              <View style={{ flex: 1, flexDirection: "row" }}>
                <Button
                  title={"Send inn vurderingen"}
                  buttonStyle={{ backgroundColor: "#48BB78" }}
                  onPress={() => submit()}
                />
              </View>
            </View>
          </View>
        </View>
      </SafeAreaView>
    );
  } else {
    return (
      <View style={globalStyles.container}>
        <Loader />
      </View>
    );
  }
};

export default AddReviewScreen;
