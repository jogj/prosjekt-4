import * as React from "react";
import { useSelector } from "react-redux";
import { Text, View } from "react-native";
import { RootState } from "../store/store";
import { Button } from "react-native-elements";
import Rating from "../components/Rating";
import { SafeAreaView } from "react-native-safe-area-context";
import globalStyles from "../constants/Styles";
import GoBackButton from "../components/GoBackButton";
import { Loader } from "../components/Loader";
import { StackNavigationProp } from "@react-navigation/stack";
import { StackParamList } from "../App";

type StudentCityScreenNavigationProp = StackNavigationProp<
  StackParamList,
  "StudentCity"
>;

/*
 * Screen for displaying details about the student city
 */
const StudentCityScreen = ({
  navigation,
}: {
  navigation: StudentCityScreenNavigationProp;
}) => {
  const { remoteData } = useSelector((state: RootState) => {
    return {
      remoteData: state.studentCityState.studentCity,
    };
  });

  const studentCity = remoteData.phase == "SUCCESS" ? remoteData.data : null;

  if (!studentCity)
    return (
      <View style={globalStyles.container}>
        <Loader />
      </View>
    );

  const ratings = [
    [studentCity.vurderingTotal, "total"],
    [studentCity.vurderingPris, "pris"],
    [studentCity.vurderingFellesAreal, "fellesareal"],
    [studentCity.vurderingLokasjon, "lokasjon"],
    [studentCity.vurderingTilstand, "tilstand"],
  ];

  return (
    <SafeAreaView style={globalStyles.statusBar}>
      <View style={globalStyles.container}>
        <View style={{ flex: 1, flexDirection: "column" }}>
          <View
            style={{
              flex: 2,
              alignItems: "center",
              flexDirection: "row",
              justifyContent: "flex-start",
            }}
          >
            <GoBackButton navigation={navigation} />
          </View>
          <View
            style={{
              flex: 4,
              flexDirection: "column",
              justifyContent: "center",
              alignItems: "center",
              paddingBottom: 40,
            }}
          >
            <View style={{ flex: 1, flexDirection: "row" }}>
              <Text
                style={{ fontSize: 40, fontWeight: "bold", color: "white" }}
              >
                {studentCity.navn}
              </Text>
            </View>
            <View style={{ flex: 1, flexDirection: "row" }}>
              <Text
                style={{ fontSize: 20, fontWeight: "bold", color: "white" }}
              >
                {studentCity.byNavn}
              </Text>
            </View>
            <View style={{ flex: 1, flexDirection: "row" }}>
              <Text
                style={{ fontSize: 20, fontWeight: "bold", color: "#718096" }}
              >
                {`Utleier: ${studentCity.utleier}`}
              </Text>
            </View>
          </View>

          <View
            style={{
              flex: 4,
              flexDirection: "column",
              alignItems: "center",
            }}
          >
            {ratings.map((val) => {
              return (
                <Rating
                  bgcolor="transaprent"
                  rating={Number(val[0])}
                  text={val[1]}
                  key={val[1]}
                />
              );
            })}
          </View>
          <View
            style={{
              flex: 2,
              flexDirection: "column",
              justifyContent: "center",
              alignItems: "center",
              paddingBottom: 40,
            }}
          >
            <View style={{ flex: 1, flexDirection: "row" }}>
              <Text style={{ fontSize: 15, color: "#718096" }}>
                {`Vurderingene viser gjennomsnittet av ${studentCity.anmeldelserCount} anmeldelser`}
              </Text>
            </View>
            <View style={{ flex: 1, flexDirection: "row" }}>
              <Button
                title={"Legg til en vurdering"}
                onPress={() => navigation.navigate("AddReview")}
              />
            </View>
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
};

export default StudentCityScreen;
