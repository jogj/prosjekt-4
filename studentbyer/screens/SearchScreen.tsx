import * as React from "react";
import { StyleSheet } from "react-native";
import StudentCityCards from "../components/StudentCityCards";
import { View } from "react-native";
import { SearchParameters } from "../components/SearchParameters";
import { SafeAreaView } from "react-native-safe-area-context";
import globalStyles from "../constants/Styles";
import { StackNavigationProp } from "@react-navigation/stack";
import { StackParamList } from "../App";

export type SearchScreenNavigationProp = StackNavigationProp<
  StackParamList,
  "Search"
>;

/*
 * Screen for searching and scrolling in studentcities
 */
export const SearchScreen = ({
  navigation,
}: {
  navigation: SearchScreenNavigationProp;
}) => {
  return (
    <>
      <SafeAreaView style={globalStyles.statusBar}>
        <SearchParameters />
        <View style={styles.cards}>
          <StudentCityCards navigation={navigation} />
        </View>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  cards: {
    flex: 3,
    flexDirection: "column",
    width: "100%",
    backgroundColor: "#2D3748",
  },
});
