# Project 4 - Studentboliger

Studentboliger is an app created for being able to browse reviews given to studentcities based on different parametres like prices, location etc. and being able to give reviews like these yourself.

## Getting started

1. Clone the repository
2. Run `cd prosjeckt-4/studentbyer`
3. Run `yarn install`
4. Run `yarn start`

In order to test on your phone you must have the [Expo-app](https://play.google.com/store/apps/details?id=host.exp.exponent&hl=en&gl=US) installed. 

## Design

The application has a form of search with text, filtering based on city and sorting based on alphabetical order and more. The resultset is loaded dynamically and increased when the user scrolls down. The user can also click on each studentcity to go into a more detailed view where it is also possible to add reviews.

The application was developed using TypeScript, React Native (through Expo).

### Reuse of code

The first step when creating this app was deciding what code from the frontend in [project 3](https://gitlab.stud.idi.ntnu.no/it2810-h20/team-72/prosjekt-3) we wanted to reuse. Reusing the UI components was not possible due to the difference between webpages and apps. Initially we chose to reuse the Redux-setup with redux-saga as this has nothing to do with the UI and could be copied from project 3. This got the app up and running pretty fast. We eventually decided to reduce the amount of data stored in the redux store by quite a lot. In project 3 we had all studentcities and cities fetched in the store, there we could argue that we did this so we could learn more about redux, but now there is really no good reason to have that amount of data which is only used one place in the redux store. By removing the data from redux store, we reduced unnecassary complexity and made the code easier to understand and work with. We still store the filter and the page in redux, since these values are set in one component and used in a different component. We also decided to put StudentCity in the store, this studentcity is the current studentcity you are on in StudentCityScreen and in AddReviewScreen, this made it easy to get the current studentcity in both screens without passing it as props and to update it easily using redux-saga which listens to fetchStudentCity(<id>)-actions and then fetches that studentcity from the backend. 


### Overall composition

For handling the overall composition of the app we used [react-navigation](https://reactnavigation.org/) which allowed us to setup stacks of screens (our different pages). We have 4 different screens:

- Home: The screen that welcomes the user to our application and explains what it is all about, contains a button that sends the user to "Search"-screen.
<img src="images/HomeScreen.jpg" alt="HomeScreen" width="280" height="600"/>

- Search: The screen where the user can search, filter and sort the studentcities, and see the studencities that match the search. Clicking on a studentcity sends the user to "StudentCity"-screen.
<img src="images/SearchScreen.jpg" alt="SearchScreen" width="280" height="600"/>

- StudentCity: The screen which shows a detailed view of all average ratings for a studentcity. Here the user can click on "Legg til vurdering" which sends the user to "AddReview"-screen.
<img src="images/StudentCityScreen.jpg" alt="StudentCityScreen" width="280" height="600"/>

- AddReview: The screen where the user can click on the stars to give ratings to the different parameters and send in the review for the studentcity which the user came from.
<img src="images/AddReviewScreen.jpg" alt="AddReviewScreen" width="280" height="600"/>



### UI Components

We implemented all of the UI components in React Native using the [react-native-elements library](https://reactnativeelements.com/docs/). The reason for choosing specifically this one is that it was an opensource all-in-one UI kit, which meant that we didn't have to go looking for components in other libraries very often. We ended up using two UI-components from other libaries. We used [react-native-tiny-toast](https://github.com/cagdaskarademir/react-native-tiny-toast) since react-native-elements' toast only worked on Android. We also used [react-native-picker-select](https://www.npmjs.com/package/react-native-picker-select) because react-native-elements had no choicebox.

We designed the UI components very similarly to the ones from project 3, so while we couldn't copy the code line for line we could reuse a lot of the states, effects etc., and the overall visual design of the app. 

One improvement we made from project 3, based on the feedback from other students, was that we added debouncing to the searchfield so that the amount of requests sent to the API is reduced. We added debouncing using [lodash](https://lodash.com/docs/4.17.15#debounce).

Another change we made from project three is that we went from having a pagination component to having a infinite scroll list with dynamic fetching. We used a guide from [scotch](https://scotch.io/tutorials/implementing-an-infinite-scroll-list-in-react-native?fbclid=IwAR0OVG3B1HDv8DjAmemfEyM1Di-5a8AAc6l850aTmy6dp7s61LxULehiDOo) to see how we could use a `FlatList` component to display the studentCityCards and add studentcites to the list by dynamically fetching from the backend when the user scrolled to the bottom of the list.

For handling layout in React Native we used `View`s as flexboxes.

## Testing
To test the application we performed manual end2end tests on both iOS and Android. This was very time consuming, so we definitely see the reason for having a lot of automatic testing. We also ran the project in the web browser to test that our frontend would work well on different screen sizes. The backend was already well-tested from project 3, so we only needed to test that our new frontend worked as expected.
